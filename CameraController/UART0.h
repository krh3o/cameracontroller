/*
 * UART0.h
 *
 * Created: 12/23/2012 6:50:06 PM
 *  Author: Karl Waters
 */ 

#ifndef UART0_H_
#define UART0_H_

void USART0_Init( unsigned int ubrrNum );

unsigned char USART0_Receive( void );
void USART0_Transmit( unsigned char data );
unsigned char DataInReceiveBuffer( void );


#endif /* UART0_H_ */