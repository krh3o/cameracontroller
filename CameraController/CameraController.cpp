/*
 * CameraController.cpp
 *
 * Created: 12/22/2012 3:24:21 PM
 *  Author: Karl Waters
 */ 

#define F_CPU 20000000

#include <avr\interrupt.h>
#include <util\delay.h>
#include <string.h>
#include "UART0.h"

// Declarations
void CommandLineInterface( void );
void ProccessCliCommand(unsigned char* cmdString, unsigned char inputLength, void(*putcFunc)(unsigned char));
void focus( void );
void snapPhoto( void );
void openShutter( void );
void closeShutter( void );
void motionSensorEnable( char enable );
void disableInterval( void );
void enableInterval( void );

//
int intervalMatch = 0;
int intervalCount = 0;

//
void kw_putc( unsigned char data)
{
	USART0_Transmit(data);
}

//
unsigned char kw_getc(void)
{
	return USART0_Receive();
}

//
void kw_puts( const char data[])
{
	unsigned char index = 0;
	//unsigned char length = sizeof(data);

	while (data[index] != 0)
	{
		kw_putc(data[index]);
		index++;
	}
}

//
void ledEnable( unsigned char on )
{
	if (on)
	{
		PORTB |= (1<<PORTB5);
	}
	else
	{
		PORTB &= ~(1<<PORTB5);
	}
}

//
int main( void )
{
	// PB5 (Arduino LED) output low
	PORTB &= ~(1<<PORTB5);
	DDRB |= (1<<DDB5);
	
	// safer to set to input first
	// PD6, PD7, and PD2 input
	DDRD &= ~((1<<DDD6)|(1<<DDD7)|(1<<DDD2));
	// PD6 and PD7 high-z
	PORTD &= ~((1<<PORTD6)|(1<<PORTD7));
	// PD2 pull-up resistor
	PORTD |= (1<<PORTD2);
	
	// 8 = 115.2k
	// 3 = 250k
	USART0_Init( 3 );

	// Enable interrupts for UART
	sei();
	
	/*
	for( ; ; )
	{
		// Echo the received character
		kw_putc( kw_getc() ); 
	}
	*/
	
	// Process incoming serial data. Does not return.
	/* Skip the CLI and go straight to the interval shutter control
	CommandLineInterface();
	*/
	
	// ever 60 seconds, snap a photo
	intervalMatch = 60;
	enableInterval();
	
	for(;;){}

	return 0;
}

#define MAX_INPUT_LENGTH    32
#define MAX_OUTPUT_LENGTH   100

void CommandLineInterface( void )
{
	static unsigned char inputString[MAX_INPUT_LENGTH] = {0};
	unsigned char inputIndex = 0;

	unsigned char dataRx = 0;
	
	kw_puts("\r\nEntering CLI\r\n");

	for ( ;; )
	{
		dataRx = kw_getc();

		if (dataRx == '\r')
		{
			kw_puts("\r\n");
			ProccessCliCommand(inputString, MAX_INPUT_LENGTH, kw_putc);
			
			// reset cmd string buffer
			memset(inputString, 0, inputIndex);
			inputIndex = 0;
		}
		else
		{
			if (dataRx == '\n')
			{
				kw_puts("ignore newline\r\n");
				// ignore
			}
			else if (dataRx == 127)
			{
				// remove last character from input string
				if (inputIndex > 0)
				{
					inputIndex--;
					inputString[inputIndex] = 0;
					kw_putc(127);
				}
			}
			else
			{
				// Add character to input string
				if (inputIndex < MAX_INPUT_LENGTH)
				{
					inputString[inputIndex] = dataRx;
					inputIndex++;
					kw_putc(dataRx);
				}
				else
				{
					kw_puts("\r\nError! CLI input string too long.\r\n");
				}
			}
		}
	}
}

//
void ProccessCliCommand(unsigned char* cmdString, unsigned char inputLength, void(*putcFunc)(unsigned char))
{

    // f - focus
    // s - take picture
    // o - open shutter
    // c - close shutter
	// m on - motion sensor on
	// m off - motion sensor off
	// i <time> - interval in ms

    const char response_ok[] = "ok\r\n";
    const char response_error[] = "error\r\n";
    const char response_not_implemented[] = "not implemented\r\n";
    const char response_unknown[] = "unkown cmd\r\n";

    unsigned char cmdIndex = 0;
    while (cmdIndex < inputLength && cmdString[cmdIndex] != 0)
    {
        if (cmdString[cmdIndex] == 'f')
        {
            // focus
            focus();
            kw_puts(response_ok); 
        }
        else if (cmdString[cmdIndex] == 's')
        {
            // snap photo
            snapPhoto();
            kw_puts(response_ok); 
        }
        else if (cmdString[cmdIndex] == 'o')
        {
            // open shutter
			openShutter();
            kw_puts(response_ok); 
        }
        else if (cmdString[cmdIndex] == 'c')
        {
            // close shutter
			closeShutter();
            kw_puts(response_ok); 
        }
		else if (cmdString[cmdIndex] == 'm')
		{
			cmdIndex++;
			if ( 0 == strncmp((const char*)(cmdString+cmdIndex), " on", 3) )
			{
				motionSensorEnable(1);
				cmdIndex += 3;
			}
			else if ( 0 == strncmp((const char*)(cmdString+cmdIndex), " off", 4) )
			{
				motionSensorEnable(0);
				cmdIndex += 4;
			}
			else
			{
				kw_puts(response_unknown);
				kw_puts("->");
				kw_puts((const char*)(cmdString+cmdIndex));
				kw_puts("\r\n\r\n");
				return;
			}
			kw_puts(response_ok);
		}
		else if (cmdString[cmdIndex] == 'i')
		{
			// interval control - not tested w/ CLI
			// <time> not implemented yet
			if ( 0 == strncmp((const char*)(cmdString+cmdIndex), " off", 4) )
			{
				disableInterval();
				cmdIndex += 4;
			}
			else
			{
				enableInterval();
			}
			kw_puts(response_ok);
		}			
        else if (cmdString[cmdIndex] == ' ')
        {
            // ignore space
        }
        else
        {
            // Unknown command
            kw_puts(response_unknown); 
        }

        cmdIndex++;
    }
}

//
void focus( void )
{
	// red wire - focus
	// PD7 (Arduino IO7)
	
	DDRD |= (1<<DDD7);
	ledEnable(1);
	kw_puts("focus: on\r\n");
	_delay_ms(2000);
	_delay_ms(2000);
	kw_puts("focus: off\r\n");
	ledEnable(0);
	DDRD &= ~(1<<DDD7);
}

//
void snapPhoto( void )
{
    // yellow wire - shutter
	// PD6 (Arduino IO6)
	
	DDRD |= (1<<DDD6);
	ledEnable(1);
	kw_puts("snap shutter: open\r\n");
	_delay_ms(2000);
	kw_puts("snap shutter: close\r\n");
	ledEnable(0);
	DDRD &= ~(1<<DDD6);
}

//
void openShutter( void )
{
	// yellow wire - shutter
	// PD6 (Arduino IO6)
	
	kw_puts("shutter open\r\n");
	DDRD |= (1<<DDD6);
	ledEnable(1);
}

//
void closeShutter( void )
{
	// yellow wire - shutter
	// PD6 (Arduino IO6)
	
	kw_puts("shutter close\r\n");
	DDRD &= ~(1<<DDD6);
	ledEnable(0);
}

//
void disableInterval( void )
{
	TCCR1B = 0;
	
	// Disable OC interrupt
	TIMSK1 &= ~(1<<OCIE1A);
}

//
void enableInterval( void )
{
	// Inital timer value
	TCCR1A = 0;
	TCCR1B = 0;
	TCNT1 = 0;
	
	// Output compare value
	OCR1A = 15625;
	
	// turn on CTC mode
	TCCR1B |= (1<<WGM12);
	// Set timer clock to clk/1024 (b101)
	TCCR1B |= (1<<CS12) | (1<<CS10);
	
	// Enable OC interrupt
	TIMSK1 |= (1<<OCIE1A);
}

//
void motionSensorEnable( char enable )
{
	// INT0 is PORTD Pin 2
	if (enable)
	{
		// setup interrupt for low level
		EICRA &= ~((1<<ISC01)|(1<<ISC00));
		
		// Enable interrupt
		EIMSK |= (1<<INT0);
	}
	else
	{
		EIMSK &= ~(1<<INT0);
	}
}

// Interrupt for motion sensor
ISR(INT0_vect)
{
    // yellow wire - shutter
    // PD6 (Arduino IO6)
	
	// disable INT0 so we don't re-enter
	EIMSK &= ~(1<<INT0);
    
    DDRD |= (1<<DDD6);
    ledEnable(1);
    _delay_ms(2000);
    ledEnable(0);
    DDRD &= ~(1<<DDD6);
	
	// clear interrupts
	EIFR = (1<<INTF0);
	// re-enable INT0
	EIMSK |= (1<<INT0);
}

// Interrupt for interval timer
ISR(TIMER1_COMPA_vect)
{
	// yellow wire - shutter
	// PD6 (Arduino IO6)
	
	if (intervalMatch == intervalCount)
	{
		intervalCount = 0;
		DDRD |= (1<<DDD6);
		ledEnable(1);
		_delay_ms(400);
		ledEnable(0);
		DDRD &= ~(1<<DDD6);
	}
	intervalCount++;
}