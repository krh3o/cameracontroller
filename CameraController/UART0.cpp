/*
 * UART0.cpp
 */ 
// AVR306: Using the AVR UART in C
// Routines for interrupt controlled USART
// Modified by: AR 02-06-21
// Modified by: KLW 19 DEC 2012

// Includes
#include <avr\interrupt.h>

// UART Buffer Defines
// 2,4,8,16,32,64,128 or 256 bytes
#define USART_RX_BUFFER_SIZE 128
#define USART_TX_BUFFER_SIZE 128

#define USART_RX_BUFFER_MASK ( USART_RX_BUFFER_SIZE - 1 )
#if ( USART_RX_BUFFER_SIZE & USART_RX_BUFFER_MASK )
#error RX buffer size is not a power of 2
#endif

#define USART_TX_BUFFER_MASK ( USART_TX_BUFFER_SIZE - 1 )
#if ( USART_TX_BUFFER_SIZE & USART_TX_BUFFER_MASK )
#error TX buffer size is not a power of 2
#endif

// Static Variables
static unsigned char USART_RxBuf[USART_RX_BUFFER_SIZE];
static volatile unsigned char USART_RxHead;
static volatile unsigned char USART_RxTail;

static unsigned char USART_TxBuf[USART_TX_BUFFER_SIZE];
static volatile unsigned char USART_TxHead;
static volatile unsigned char USART_TxTail;

// Prototypes

// Initialize USART
void USART0_Init( unsigned int ubrrNum )
{
	// Set the baud rate
	UBRR0H = (unsigned char) (ubrrNum>>8);
	UBRR0L = (unsigned char) ubrrNum;
	
	// Enable UART receiver and transmitter
	UCSR0B = ( ( 1 << RXCIE0 ) | ( 1 << RXEN0 ) | ( 1 << TXEN0 ) );
	
	// Set frame format: 8 data 2stop
	UCSR0C = (1<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00);              //For devices with Extended IO
	//UCSR0C = (1<<URSEL)|(1<<USBS0)|(1<<UCSZ01)|(1<<UCSZ00);   //For devices without Extended IO

	// Power Reduction USART disabled
	PRR &= ~(1 << PRUSART0);
	
	// Flush receive buffer
	USART_RxTail = 0;
	USART_RxHead = 0;
	USART_TxTail = 0;
	USART_TxHead = 0;
}

/* Interrupt handlers */
ISR(USART_RX_vect)
{
	unsigned char data;
	unsigned char tmphead;

	// Read the received data
	data = UDR0;

	// Calculate buffer index
	tmphead = ( USART_RxHead + 1 ) & USART_RX_BUFFER_MASK;

	if ( tmphead == USART_RxTail )
	{
		// ERROR! Receive buffer overflow
		// Return early, byte is lost
		// Advancing the RxHead would wipe the whole buffer
		return;
	}
	
	// Store received data in buffer
	USART_RxBuf[tmphead] = data;

	// Store new index after data is stored to array and overflow is checked for
	USART_RxHead = tmphead;
}

//
// USART0_TX_interrupt
ISR(USART_UDRE_vect)
{
	unsigned char tmptail;

	// Check if all data is transmitted
	if ( USART_TxHead != USART_TxTail )
	{
		// Calculate buffer index
		tmptail = ( USART_TxTail + 1 ) & USART_TX_BUFFER_MASK;
		
		// Start transmission
		UDR0 = USART_TxBuf[tmptail];

		// Store new index
		USART_TxTail = tmptail;
	}
	else
	{
		// Disable UDRE interrupt
		UCSR0B &= ~(1<<UDRIE0);
	}
}

// Read and write functions
unsigned char USART0_Receive( void )
{
	unsigned char tmptail;
	
	// Wait for incomming data
	while ( USART_RxHead == USART_RxTail );

	// Calculate buffer index
	tmptail = ( USART_RxTail + 1 ) & USART_RX_BUFFER_MASK;
	// Store new index
	USART_RxTail = tmptail;
	
	// Return data
	return USART_RxBuf[tmptail];
}

void USART0_Transmit( unsigned char data )
{
	unsigned char tmphead;

	// Calculate buffer index
	tmphead = ( USART_TxHead + 1 ) & USART_TX_BUFFER_MASK;
	// Wait for free space in buffer
	while ( tmphead == USART_TxTail );

	// Store data in buffer
	USART_TxBuf[tmphead] = data;
	// Store new index
	USART_TxHead = tmphead;

	// Enable UDRE interrupt
	UCSR0B |= (1<<UDRIE0);
}

unsigned char DataInReceiveBuffer( void )
{
	// Return 0 (FALSE) if the receive buffer is empty
	return ( USART_RxHead != USART_RxTail );
}
